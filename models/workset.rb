class Occam
  class Workset < ActiveRecord::Base
    require 'json'

    # Fields

    # id        - Unique identifier.

    # name      - The name of the experiment.
    validates :name, :presence => true
    validates :name, :uniqueness => {
      :case_sensitive => false
    }

    # tags      - The semicolon separated list of tags.

    has_one :workflow

    # Experiment has a workset_id foreign key
    has_many :experiments

    # Worksets can have many collaborators
    # A Collaboration is a join table between worksets and accounts
    has_many :collaborations
    has_many :accounts, :through => :collaborations

    has_many :groups

    # Returns true when the given account is collaborating (includes owner)
    def is_collaborator?(account)
      account && self.account == account || self.accounts.include?(account)
    end

    # private - Boolean: whether or not the experiment is locked to creators/collaborators

    # Determines whether or not the account given can see this workset
    def can_view?(account)
      if self.private == 1
        self.is_collaborator? account
      else
        true
      end
    end

    # Determines whether or not the account given can edit this workset
    def can_edit?(account)
      self.is_collaborator? account
    end

    # configuration - The simulator configuration JSON.
    def configuration
      document_id = BSON::ObjectId.from_string(self.configuration_document_id)

      document = ::Configuration.first(:id => document_id)

      if document
        document.serializable_hash
      end
    end

    def configuration=(value)
      if self.configuration_document_id
        document_id = BSON::ObjectId.from_string(self.configuration_document_id)
        document = ::Configuration.first(:id => document_id)
      end

      if document
        document.attributes = value
        document.save
      else
        document = ::Configuration.create(value)
        self.configuration_document_id = document.id.to_s
        self.save
      end
      document
    end

    # account   - The account that created this experiment.
    belongs_to :account

    # recipe - The Recipe this experiment was spawned from (if any).
    belongs_to :recipe

    # forked_from - The experiment this one was based off of (if any).
    belongs_to :forked_from, :class_name => "Workset"

    def results
      if self.results_document_id
        document_id = BSON::ObjectId.from_string(self.results_document_id)
        document = Result.first(:id => document_id)
      end

      if document
        document.serializable_hash
      end
    end

    def results=
    end

    def self.all_tags(with = "")
      with ||= ""
      self.all.to_a.map{|ex| ex.tags[1..-2].split(";")}.flatten.uniq.select{|tag| tag.match(Regexp.escape(with))}
    end

    def self.all_tags_json(with = "")
      self.all_tags(with).to_json
    end

    def self.with_tag(tag)
      Workset.where('tags LIKE ?', "%;#{tag};%")
    end

    def forked_us
      Workset.where(:forked_from_id => self.id).select([:name, :id])
    end

    def initialize(options = {}, *args)
      # Ensure a leading and ending ',' in the tag CSV
      options[:tags] ||= ""
      if options[:tags].is_a? Array
        options[:tags] = ";#{options[:tags].join(';')};"
      end

      if !options[:tags].start_with?(";")
        options[:tags] = ";#{options[:tags]}"
      end
      if !options[:tags].end_with?(";")
        options[:tags] = "#{options[:tags]};"
      end

      super options, *args
    end
  end
end
