class Occam
  class Input < ActiveRecord::Base
    # Fields

    # id - Unique identifier.

    # Associations

    # input_document_id - The string containing the id for the input document.

    # Returns the input in the given format. Defaults to yielding a Hash object
    # describing the input.
    #
    # format:
    #   :hash => Gives a Hash object. (default)
    #   :json => Gives a string containing a valid JSON representation.
    def input(format = :hash)
      if self.input_document_id
        document_id = BSON::ObjectId.from_string(self.input_document_id)
        document = Occam::InputDocument.first(:id => document_id)
      end

      if document
        case format
        when :hash
          document.serializable_hash
        when :json
          document.to_json
        end
      end
    end

    def input=(value)
      if self.input_document_id
        document_id = BSON::ObjectId.from_string(self.input_document_id)
        document = Occam::InputDocument.first(:id => document_id)
      end

      if document
        document.attributes = value
        document.save
      else
        document = Occam::InputDocument.create(value)
        self.input_document_id = document.id.to_s
        self.save
      end
      document
    end

    # job_id - The job this input belongs to.
    belongs_to :job

    # configuration_id - The configuration this input corresponds to
    belongs_to :configuration
  end
end
