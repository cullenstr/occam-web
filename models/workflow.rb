class Occam
  class Workflow < ActiveRecord::Base
    # Fields

    # name

    # Associations

    # A Workflow may be attached to either a Workset, Group, or Experiment

    # group_id
    belongs_to :group
    # experiment_id
    belongs_to :experiment
    # workset_id
    belongs_to :workset

    # It keeps a record of who created it. Not necessarily the Workset owner.
    # account_id
    belongs_to :account

    # It keeps record of what workset it was cloned from, if any.
    # forked_id
    belongs_to :workflow, :foreign_key => :forked_id

    # Foreign Associations

    # Objects are connected together through many Connection records.
    has_many :connections

    # Provide an enumerator for each object in the workflow
    def objects
      self.connections.select(:occam_object_id).map do |connection|
        connection.object
      end
    end

    # Provides all initial connections
    def tail_connections
      self.connections.where(:connection_id => nil)
    end
  end
end
