class Occam
  class Binary < ActiveRecord::Base
    # Fields

    # id           - Unique identifier

    # name         - Name of the binary

    # object       - The object it belongs to
    belongs_to :object, :foreign_key => :occam_object_id
  end
end
