class Occam
  class Configuration < ActiveRecord::Base
    require 'json'

    # Associations

    belongs_to :object,
               :foreign_key => :occam_object_id

    # Returns the schema in the given format. Defaults to yielding a Hash object
    # describing the schema.
    #
    # format:
    #   :hash => Gives a Hash object. (default)
    #   :json => Gives a string containing a valid JSON representation.
    def schema(format = :hash)
      if self.schema_document_id
        document_id = BSON::ObjectId.from_string(self.schema_document_id)
        document = ConfigurationSchema.first(:id => document_id)
      end

      if document
        case format
        when :hash
          document.serializable_hash
        when :json
          document.to_json
        end
      end
    end
  end
end
