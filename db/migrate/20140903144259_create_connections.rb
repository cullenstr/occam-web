class CreateConnections < ActiveRecord::Migration
  def up
    create_table :connections do |t|
      t.integer :workflow_id
      t.integer :connection_id
      t.integer :occam_object_id

      t.integer :co_runnable

      t.integer :object_output_id
      t.integer :object_input_id

      t.string  :creates_object_of_type
    end
  end

  def down
    drop_table :connections
  end
end
