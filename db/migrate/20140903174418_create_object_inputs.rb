class CreateObjectInputs < ActiveRecord::Migration
  def up
    create_table :object_inputs do |t|
      # The Object id (foreign key)
      t.integer :occam_object_id

      # The group for this object
      t.string  :object_group

      # The type of object
      t.string  :object_type

      # Whether or not this object is a corunning object
      t.integer :fifo
    end
  end

  def down
    drop_table :object_inputs
  end
end
