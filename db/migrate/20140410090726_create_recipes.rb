class CreateRecipes < ActiveRecord::Migration
  def up
    create_table :recipes do |t|
      t.string  :name
      t.string  :section
      t.text    :description

      t.integer :occam_object_id
      t.string  :configuration_document_id
    end
  end

  def down
    drop_table :recipes
  end
end
