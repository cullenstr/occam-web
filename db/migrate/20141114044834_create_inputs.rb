class CreateInputs < ActiveRecord::Migration
  def change
    create_table :inputs do |t|
      t.string  :input_document_id

      t.integer :configuration_id

      t.integer :job_id

      t.timestamps
    end
  end
end
