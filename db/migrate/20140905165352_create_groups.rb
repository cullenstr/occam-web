class CreateGroups < ActiveRecord::Migration
  def up
    create_table :groups do |t|
      t.string  :name

      t.text    :tags

      t.integer :occam_object_id
      t.string  :configuration_document_id

      t.integer :workset_id

      t.integer :account_id
      t.integer :forked_from_id

      t.integer :recipe_id

      t.boolean :private

      t.timestamps
    end

    add_index :groups, :forked_from_id
  end

  def down
    drop_table :groups
  end
end
