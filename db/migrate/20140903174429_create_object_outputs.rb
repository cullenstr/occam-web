class CreateObjectOutputs < ActiveRecord::Migration
  def up
    create_table :object_outputs do |t|
      # The Object id (foreign key)
      t.integer :occam_object_id

      # The group for this object
      t.string  :object_group

      # The type of object
      t.string  :object_type

      # The file containing the schema in the script repo (if any)
      t.string  :schema_file

      # The foreign key for the realized output schema
      t.string  :schema_document_id

      # The file containing the produced output (if any)
      t.string  :file

      # Whether or not this is a corunning object.
      t.integer :fifo
    end
  end

  def down
    drop_table :object_outputs
  end
end
