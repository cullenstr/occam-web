class CreateObjects < ActiveRecord::Migration
  def up
    create_table :objects do |t|
      t.string   :name
      t.string   :object_type

      t.text     :tags
      t.text     :description
      t.text     :authors
      t.string   :organization
      t.string   :license
      t.string   :website
      t.string   :documentation

      t.integer  :built

      t.string   :binary_path
      t.string   :script_path
      t.string   :package_type

      t.text     :self_metadata
      t.text     :install_metadata
      t.text     :build_metadata
      t.text     :run_metadata
      t.text     :init_metadata

      t.integer  :runs_binaries

      t.string   :local_path
      t.string   :group

      t.integer  :active

      t.string   :input_schema_document_id
      t.string   :output_schema_document_id

      t.datetime :published
      t.datetime :updated
    end
  end

  def down
    drop_table :objects
  end
end
