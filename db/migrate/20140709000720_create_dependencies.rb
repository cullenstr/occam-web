class CreateDependencies < ActiveRecord::Migration
  def up
    create_table :dependencies do |t|
      t.integer :depends_on_id
      t.integer :dependant_id

      t.timestamps
    end
  end

  def down
    drop_table :dependencies
  end
end
