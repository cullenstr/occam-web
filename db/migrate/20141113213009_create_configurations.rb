class CreateConfigurations < ActiveRecord::Migration
  def change
    create_table :configurations do |t|
      t.string  :name
      t.string  :file
      t.string  :schema_file
      t.string  :schema_document_id

      t.integer :occam_object_id

      t.timestamps
    end
  end
end
