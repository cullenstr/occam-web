require_relative "helper"
require_model "account"

describe Occam::Account do
  describe "#initialize" do
    it "should not allow creation with an existing username" do
      Occam::Account.create(:username => "wilkie",
                            :password => "foo")

      account = Occam::Account.create(:username => "wilkie",
                                      :password => "foo")

      account.errors.count.wont_equal 0
    end

    it "should not allow a blank password" do
      account = Occam::Account.create(:username => "wilkie",
                                      :password => "")

      account.errors.count.wont_equal 0
    end

    it "should create an account when given a username and password" do
      account = Occam::Account.create(:username => "wilkie",
                                      :password => "foobar")

      account.errors.count.must_equal 0
    end

    it "should create an account as inactive when moderate_accounts is set" do
      Occam::System.first.update_attributes(:moderate_accounts => true)

      a = Occam::Account.create(:username => "jane",
                                :password => "foobar")

      a.active.must_equal 0
    end

    it "should create an account as active when moderate_accounts is clear" do
      Occam::System.first.update_attributes(:moderate_accounts => false)

      a = Occam::Account.create(:username => "jane",
                                :password => "foobar")

      a.active.must_equal 1
    end

    it "should give the administrator role to the first account" do
      Occam::System.first.update_attributes(:moderate_accounts => false)

      a = Occam::Account.create(:username => "jane",
                                :password => "foobar")

      a.has_role?(:administrator).must_equal true
    end

    it "should not give the administrator role to the second account" do
      Occam::System.first.update_attributes(:moderate_accounts => false)

      a = Occam::Account.create(:username => "jane",
                                :password => "foobar")

      b = Occam::Account.create(:username => "wilkie",
                                :password => "foobar")

      b.has_role?(:administrator).must_equal false
    end
  end

  describe "::all_inactive" do
    it "should list only inactive accounts" do
      Occam::System.first.update_attributes(:moderate_accounts => true)

      a = Occam::Account.create(:username => "jane",
                                :password => "foobar")

      a.active = true
      a.save

      b = Occam::Account.create(:username => "wilkie",
                                :password => "foobar")

      Occam::Account.all_inactive.count.must_equal 1
      Occam::Account.all_inactive.first.username.must_equal "wilkie"
    end
  end

  describe "::all_active" do
    it "should list only active accounts" do
      Occam::System.first.update_attributes(:moderate_accounts => true)

      a = Occam::Account.create(:username => "jane",
                                :password => "foobar")

      a.active = true
      a.save

      b = Occam::Account.create(:username => "wilkie",
                                :password => "foobar")

      Occam::Account.all_active.count.must_equal 1
      Occam::Account.all_active.first.username.must_equal "jane"
    end
  end

  describe "#name" do
    it "should use display name as the first priority" do
      a = Occam::Account.create(:username => "jane",
                                :display_name => "Jane Mary",
                                :password => "foobar")
      a.name.must_equal "Jane Mary"
    end

    it "should use username if display name is not set" do
      a = Occam::Account.create(:username => "jane",
                                :password => "foobar")
      a.name.must_equal "jane"
    end
  end

  describe "#avatar_url" do
    it "should use gravatar when email is set" do
      a = Occam::Account.create(:username => "jane",
                                :email    => "jane@janemary.com",
                                :password => "foobar")
      a.avatar_url(48).must_match /^https:\/\/www.gravatar.com\/avatar\//
    end

    it "should pass the size to gravatar when email is set" do
      a = Occam::Account.create(:username => "jane",
                                :email    => "jane@janemary.com",
                                :password => "foobar")
      a.avatar_url(57).must_match /\?size=57$/
    end

    it "should use a default image when email is not set" do
      a = Occam::Account.create(:username => "jane",
                                :password => "foobar")
      a.avatar_url(48).must_equal "/images/person.png"
    end

    it "should use the default large image when size is large" do
      a = Occam::Account.create(:username => "jane",
                                :password => "foobar")
      a.avatar_url(96).must_equal "/images/person_large.png"
    end
  end
end
