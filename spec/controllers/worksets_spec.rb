require_relative 'helper'

describe Occam do
  describe "Worksets Controller" do
    describe "GET /worksets" do
      it "should pass an array of all worksets to the view" do
        Occam::Workset.create(:name => "foo")
        Occam::Workset.create(:name => "bar")
        Occam::Workset.create(:name => "baz")

        Occam.any_instance.expects(:render).with(
          anything,
          anything,
          has_local_of_type(:worksets => ActiveRecord::Relation::ActiveRecord_Relation_Occam_Workset)
        )

        get '/worksets'
      end

      it "should query and pass the worksets to the view" do
        Occam::Workset.create(:name => "foo")
        workset = Occam::Workset.create(:name => "bar")
        Occam::Workset.create(:name => "baz")

        Occam.any_instance.expects(:render).with(
          anything,
          anything,
          has_local_includes(:worksets, workset)
        )

        get '/worksets'
      end

      it "should return 200" do
        get '/worksets'

        last_response.status.must_equal 200
      end

      it "should render worksets/index.haml" do
        Occam.any_instance.expects(:render).with(
          anything,
          :"worksets/index",
          anything
        )

        get '/worksets'
      end

      it "should list the worksets with the given tag if given" do
        workset    = Occam::Workset.create(:name => "foo", :tags => ["a"])
        eliminated = Occam::Workset.create(:name => "bar", :tags => ["b"])
        Occam::Workset.create(:name => "baz", :tags => ["a"])

        Occam.any_instance.expects(:render).with(
          anything,
          anything,
          has_local_includes(:worksets, workset)
        )

        get '/worksets?tag=a'
      end

      it "should not list the worksets without the given tag if given" do
        workset    = Occam::Workset.create(:name => "foo", :tags => ["a"])
        eliminated = Occam::Workset.create(:name => "bar", :tags => ["b"])
        Occam::Workset.create(:name => "baz", :tags => ["a"])

        Occam.any_instance.expects(:render).with(
          anything,
          anything,
          has_not_local_includes(:worksets, eliminated)
        )

        get '/worksets?tag=a'
      end

      it "should pass the given tag to the view" do
        Occam::Workset.create(:name => "foo", :tags => ["a"])
        Occam::Workset.create(:name => "bar", :tags => ["b"])
        Occam::Workset.create(:name => "baz", :tags => ["a"])

        Occam.any_instance.expects(:render).with(
          anything,
          anything,
          has_local(:tag, 'a')
        )

        get '/worksets?tag=a'
      end

      it "should pass all known tags to the view" do
        Occam::Workset.create(:name => "foo", :tags => ["a"])
        Occam::Workset.create(:name => "bar", :tags => ["b"])
        Occam::Workset.create(:name => "baz", :tags => ["a"])

        Occam.any_instance.expects(:render).with(
          anything,
          anything,
          has_local_includes(:tags, 'a')
        )

        get '/worksets'
      end
    end

    describe "POST /worksets" do
      it "should return 406 if user is not logged in" do
        post '/worksets', { }

        last_response.status.must_equal 406
      end

      it "should return 422 upon error" do
        login_as('wilkie')

        post '/worksets', {
          "name" => ''
        }

        last_response.status.must_equal 422
      end

      it "should redirect to workset upon success" do
        login_as('wilkie')

        post '/worksets', {
          "name" => 'foo'
        }

        workset = Occam::Workset.find_by(:name => "foo")

        last_response.location.must_equal(
          "http://example.org/worksets/#{workset.id}"
        )
      end

      it "should return 302 upon success" do
        login_as('wilkie')

        post '/worksets', {
          "name" => 'foo'
        }

        last_response.status.must_equal 302
      end
    end

    describe "GET /worksets/tags" do
      it "should return a json content type" do
        get '/worksets/tags'

        content_type.must_match /application\/json/
      end

      it "should return json containing the tags" do
        Occam::Workset.create(:name => "foo", :tags => ["a"])
        Occam::Workset.create(:name => "bar", :tags => ["b"])
        Occam::Workset.create(:name => "baz", :tags => ["a"])

        get '/worksets/tags'

        JSON::parse(last_response.body).must_include "a"
      end

      it "should return json containing the tags matching given term" do
        Occam::Workset.create(:name => "foo", :tags => ["apple"])
        Occam::Workset.create(:name => "bar", :tags => ["banana"])
        Occam::Workset.create(:name => "baz", :tags => ["snapple"])

        get '/worksets/tags?term=app'

        JSON::parse(last_response.body).wont_include "banana"
      end
    end

    describe "GET /worksets/:id/fork" do
    end

    describe "POST /worksets/:id" do
      it "should return 404 when workset is not found" do
        Occam.any_instance.stubs(:markdown)

        post '/worksets/asdf', {}

        last_response.status.must_equal 404
      end

      it "should return 406 if the logged in user isn't owner/collaborator" do
        a = Occam::Account.create(:username => "wilkie",
                                  :password => "foo")

        workset = Occam::Workset.create(:name => "foo",
                                        :account => a)

        login_as('jane')

        post "/worksets/#{workset.id}", {
          "name" => "bar"
        }

        last_response.status.must_equal 406
      end

      it "should return 406 if nobody is logged in" do
        a = Occam::Account.create(:username => "wilkie",
                                  :password => "foo")

        workset = Occam::Workset.create(:name => "foo",
                                        :account => a)

        post "/worksets/#{workset.id}", {
          "name" => "bar"
        }

        last_response.status.must_equal 406
      end

      it "should return 302 when successful" do
        a = login_as('wilkie')

        workset = Occam::Workset.create(:name => "foo",
                                        :account => a)

        post "/worksets/#{workset.id}", {
          "name" => "bar"
        }

        last_response.status.must_equal 302
      end

      it "should redirect to workset when successful" do
        a = login_as('wilkie')

        workset = Occam::Workset.create(:name => "foo",
                                        :account => a)

        post "/worksets/#{workset.id}", {
          "name" => "bar"
        }

        last_response.location.must_equal(
          "http://example.org/worksets/#{workset.id}"
        )
      end

      it "should allow collaborators to change the name" do
        b = Occam::Account.create(:username => "jane",
                                  :password => "foo")

        a = login_as('wilkie')

        workset = Occam::Workset.create(:name     => "foo",
                                        :accounts => [a],
                                        :account  => b)

        post "/worksets/#{workset.id}", {
          "name" => "bar"
        }

        Occam::Workset.find_by(:id => workset.id).name.must_equal "bar"
      end

      it "should allow the owner to change the tags" do
        b = Occam::Account.create(:username => "jane",
                                  :password => "foo")

        a = login_as('wilkie')

        workset = Occam::Workset.create(:name     => "foo",
                                        :tags     => ["a", "b"],
                                        :accounts => [a],
                                        :account  => b)

        post "/worksets/#{workset.id}", {
          "tags" => ";c;d;"
        }

        Occam::Workset.find_by(:id => workset.id).tags.must_equal ";c;d;"
      end

      it "should allow the owner to change the private/public status" do
        b = Occam::Account.create(:username => "jane",
                                  :password => "foo")

        a = login_as('wilkie')

        workset = Occam::Workset.create(:name     => "foo",
                                        :private  => 0,
                                        :accounts => [a],
                                        :account  => b)

        post "/worksets/#{workset.id}", {
          "private" => "1"
        }

        Occam::Workset.find_by(:id => workset.id).private.must_equal 1
      end

      it "should allow the owner to change the name" do
        a = login_as('wilkie')

        workset = Occam::Workset.create(:name => "foo",
                                        :account => a)

        post "/worksets/#{workset.id}", {
          "name" => "bar"
        }

        Occam::Workset.find_by(:id => workset.id).name.must_equal "bar"
      end

      it "should allow the owner to change the tags" do
        a = login_as('wilkie')

        workset = Occam::Workset.create(:name => "foo",
                                        :tags => ["a", "b"],
                                        :account => a)

        post "/worksets/#{workset.id}", {
          "tags" => ";c;d;"
        }

        Occam::Workset.find_by(:id => workset.id).tags.must_equal ";c;d;"
      end

      it "should allow the owner to change the private/public status" do
        a = login_as('wilkie')

        workset = Occam::Workset.create(:name => "foo",
                                        :private => 0,
                                        :account => a)

        post "/worksets/#{workset.id}", {
          "private" => "1"
        }

        Occam::Workset.find_by(:id => workset.id).private.must_equal 1
      end
    end

    describe "GET /worksets/:id" do
      it "should return 404 when the workset isn't found" do
        Occam.any_instance.stubs(:markdown)

        get '/worksets/asdf'

        last_response.status.must_equal 404
      end

      it "should return 406 when the workset can't be viewed" do
        owner = Occam::Account.create(:username => "jane",
                                      :password => "foo")

        login_as('wilkie')

        workset = Occam::Workset.create(:name => "foo",
                                        :private => 1,
                                        :account => owner)

        get "/worksets/#{workset.id}"

        last_response.status.must_equal 406
      end

      it "should return 200 when private workset is viewed by the owner" do
        owner = Occam::Account.create(:username => "jane",
                                      :password => "foo")

        login_as('jane', owner)

        workset = Occam::Workset.create(:name     => "foo",
                                        :private  => 1,
                                        :workflow => Occam::Workflow.new,
                                        :account  => owner)

        get "/worksets/#{workset.id}"

        last_response.status.must_equal 200
      end

      it "should return 200 when private workset is viewed by collaborator" do
        owner = Occam::Account.create(:username => "jane",
                                      :password => "foo")

        a = login_as('wilkie')

        workset = Occam::Workset.create(:name     => "foo",
                                        :private  => 1,
                                        :accounts => [a],
                                        :workflow => Occam::Workflow.new,
                                        :account  => owner)

        get "/worksets/#{workset.id}"

        last_response.status.must_equal 200
      end

      it "should render worksets/show.haml" do
        owner = login_as('wilkie')

        workset = Occam::Workset.create(:name     => "foo",
                                        :workflow => Occam::Workflow.new,
                                        :account  => owner)

        Occam.any_instance.expects(:render).with(
          anything,
          :"worksets/show",
          anything
        )

        get "/worksets/#{workset.id}"
      end

      it "should pass the workset to the view" do
        owner = Occam::Account.create(:username => "jane",
                                      :password => "foo")

        workset = Occam::Workset.create(:name => "foo",
                                        :workflow => Occam::Workflow.new,
                                        :account  => owner)

        Occam.any_instance.expects(:render).with(
          anything,
          anything,
          has_local(:workset => workset)
        )

        get "/worksets/#{workset.id}"
      end

      it "should pass the workflow to the view" do
        owner = Occam::Account.create(:username => "jane",
                                      :password => "foo")

        workset = Occam::Workset.create(:name => "foo",
                                        :workflow => Occam::Workflow.new,
                                        :account  => owner)

        Occam.any_instance.expects(:render).with(
          anything,
          anything,
          has_local(:workflow => workset.workflow)
        )

        get "/worksets/#{workset.id}"
      end

      it "should pass the owning account to the view" do
        owner = Occam::Account.create(:username => "jane",
                                      :password => "foo")

        workset = Occam::Workset.create(:name => "foo",
                                        :workflow => Occam::Workflow.new,
                                        :account  => owner)

        Occam.any_instance.expects(:render).with(
          anything,
          anything,
          has_local(:account => owner)
        )

        get "/worksets/#{workset.id}"
      end

      it "should pass the list of groups to the view" do
        owner = Occam::Account.create(:username => "jane",
                                      :password => "foo")

        workset = Occam::Workset.create(:name => "foo",
                                        :groups => [Occam::Group.new(:name => 'a'),
                                                    Occam::Group.new(:name => 'b'),
                                                    Occam::Group.new(:name => 'c')],
                                        :workflow => Occam::Workflow.new,
                                        :account  => owner)

        Occam.any_instance.expects(:render).with(
          anything,
          anything,
          has_local_of_type(:groups => ActiveRecord::Relation::ActiveRecord_Relation_Occam_Group)
        )

        get "/worksets/#{workset.id}"
      end

      it "should pass the list of graphs to the view" do
        owner = Occam::Account.create(:username => "jane",
                                      :password => "foo")

        workset = Occam::Workset.create(:name => "foo",
                                        :workflow => Occam::Workflow.new,
                                        :account  => owner)

        graph = Occam::Graph.create(:workset_id => workset.id,
                                    :data       => {:title => 'foo'},
                                    :type       => 'bars')

        Occam.any_instance.expects(:render).with(
          anything,
          anything,
          has_local_of_type(:graphs => MongoMapper::Plugins::Querying::DecoratedPluckyQuery)
        )

        get "/worksets/#{workset.id}"
      end

      it "should pass the list of collaborators to the view" do
        owner = Occam::Account.create(:username => "jane",
                                      :password => "foo")

        a = Occam::Account.create(:username => "wilkie",
                                  :password => "foo")

        workset = Occam::Workset.create(:name => "foo",
                                        :accounts => [a],
                                        :workflow => Occam::Workflow.new,
                                        :account  => owner)

        graph = Occam::Graph.create(:workset_id => workset.id, :data => {:title => 'foo'})

        Occam.any_instance.expects(:render).with(
          anything,
          anything,
          has_local_of_type(:collaborators =>
            ActiveRecord::Associations::CollectionProxy::ActiveRecord_Associations_CollectionProxy_Occam_Account)
        )

        get "/worksets/#{workset.id}"
      end
    end

    describe "POST /worksets/:id/graphs" do
      it "should return 404 when the workset is not found" do
        Occam.any_instance.stubs(:markdown)

        post "/worksets/asdf/graphs", {
          "data" => {"title" => "foo"}.to_json,
          "type" => "bars"
        }

        last_response.status.must_equal 404
      end

      it "should return 406 if the logged in user isn't owner/collaborator" do
        a = Occam::Account.create(:username => "wilkie",
                                  :password => "foo")

        workset = Occam::Workset.create(:name => "foo",
                                        :account => a)

        login_as('jane')

        post "/worksets/#{workset.id}/graphs", {
          "data" => {"title" => "foo"}.to_json,
          "type" => "bars"
        }

        last_response.status.must_equal 406
      end

      it "should return 406 if nobody is logged in" do
        a = Occam::Account.create(:username => "wilkie",
                                  :password => "foo")

        workset = Occam::Workset.create(:name => "foo",
                                        :account => a)

        post "/worksets/#{workset.id}/graphs", {
          "data" => {"title" => "foo"}.to_json,
          "type" => "bars"
        }

        last_response.status.must_equal 406
      end

      it "should return 200 upon success" do
        a = login_as('wilkie')

        workset = Occam::Workset.create(:name => "foo",
                                        :account => a)

        post "/worksets/#{workset.id}/graphs", {
          "data" => {"title" => "foo"}.to_json,
          "type" => "bars"
        }

        last_response.status.must_equal 200
      end

      it "should store the graph upon success" do
        a = login_as('wilkie')

        workset = Occam::Workset.create(:name => "foo",
                                        :account => a)

        post "/worksets/#{workset.id}/graphs", {
          "data" => {"title" => "foo"}.to_json,
          "type" => "bars"
        }

        Occam::Graph.where(:workset_id => workset.id).count.must_equal 1
      end
    end

    describe "GET /worksets/:id/graphs/:graph_id" do
      it "should return 404 when the workset is not found" do
        Occam.any_instance.stubs(:markdown)

        get "/worksets/asdf/graphs/asdf"

        last_response.status.must_equal 404
      end

      it "should return 406 if workset is private and logged in user isn't owner/collaborator" do
        a = Occam::Account.create(:username => "wilkie",
                                  :password => "foo")

        workset = Occam::Workset.create(:name => "foo",
                                        :private => 1,
                                        :account => a)

        graph = Occam::Graph.create(:workset_id => workset.id,
                                    :data       => {:title => 'foo'},
                                    :type       => 'bars')

        login_as('jane')

        get "/worksets/#{workset.id}/graphs/#{graph.id}"

        last_response.status.must_equal 406
      end

      it "should return 406 if workset is private and nobody is logged in" do
        a = Occam::Account.create(:username => "wilkie",
                                  :password => "foo")

        workset = Occam::Workset.create(:name => "foo",
                                        :private => 1,
                                        :account => a)

        graph = Occam::Graph.create(:workset_id => workset.id,
                                    :data       => {:title => 'foo'},
                                    :type       => 'bars')

        get "/worksets/#{workset.id}/graphs/#{graph.id}"

        last_response.status.must_equal 406
      end

      it "should return 200 upon success" do
        a = login_as('wilkie')

        workset = Occam::Workset.create(:name => "foo",
                                        :account => a)

        graph = Occam::Graph.create(:workset_id => workset.id,
                                    :data       => {:title => 'foo'},
                                    :type       => 'bars')

        get "/worksets/#{workset.id}/graphs/#{graph.id}"

        last_response.status.must_equal 200
      end

      it "should render results/graph.haml" do
        a = login_as('wilkie')

        workset = Occam::Workset.create(:name => "foo",
                                        :account => a)

        graph = Occam::Graph.create(:workset_id => workset.id,
                                    :data       => {:title => 'foo'},
                                    :type       => 'bars')

        Occam.any_instance.expects(:render).with(
          anything,
          :"results/graph",
          anything
        )

        get "/worksets/#{workset.id}/graphs/#{graph.id}"
      end

      it "should pass the graph to the view" do
        a = login_as('wilkie')

        workset = Occam::Workset.create(:name => "foo",
                                        :account => a)

        graph = Occam::Graph.create(:workset_id => workset.id,
                                    :data       => {:title => 'foo'},
                                    :type       => 'bars')

        Occam.any_instance.expects(:render).with(
          anything,
          anything,
          has_local(:graph, graph)
        )

        get "/worksets/#{workset.id}/graphs/#{graph.id}"
      end
    end

    describe "GET /worksets/:id/results" do
      it "should return 404 when the workset is not found" do
        Occam.any_instance.stubs(:markdown)

        get "/worksets/asdf/results"

        last_response.status.must_equal 404
      end

      it "should return 406 if workset is private and logged in user isn't owner/collaborator" do
        a = Occam::Account.create(:username => "wilkie",
                                  :password => "foo")

        output_schema = Occam::OutputSchema.create

        workflow = Occam::Workflow.new(:connections => [
          Occam::Connection.new(:object => Occam::Object.new(
            :name => "simulator",
            :output_schema_document_id => output_schema.id.to_s
          ))
        ])

        workset = Occam::Workset.create(:name     => "foo",
                                        :private  => 1,
                                        :workflow => workflow,
                                        :account  => a)

        login_as('jane')

        get "/worksets/#{workset.id}/results"

        last_response.status.must_equal 406
      end

      it "should return 406 if workset is private and nobody is logged in" do
        a = Occam::Account.create(:username => "wilkie",
                                  :password => "foo")

        output_schema = Occam::OutputSchema.create

        workflow = Occam::Workflow.new(:connections => [
          Occam::Connection.new(:object => Occam::Object.new(
            :name => "simulator",
            :output_schema_document_id => output_schema.id.to_s
          ))
        ])

        workset = Occam::Workset.create(:name => "foo",
                                        :private => 1,
                                        :workflow => workflow,
                                        :account => a)

        get "/worksets/#{workset.id}/results"

        last_response.status.must_equal 406
      end

      it "should return 200 upon success" do
        a = login_as('wilkie')

        output_schema = Occam::OutputSchema.create

        workflow = Occam::Workflow.new(:connections => [
          Occam::Connection.new(:object => Occam::Object.new(
            :name => "simulator",
            :output_schema_document_id => output_schema.id.to_s
          ))
        ])

        workset = Occam::Workset.create(:name => "foo",
                                        :workflow => workflow,
                                        :account => a)

        get "/worksets/#{workset.id}/results"

        last_response.status.must_equal 200
      end

      it "should render results/show.haml" do
        a = login_as('wilkie')

        output_schema = Occam::OutputSchema.create

        workflow = Occam::Workflow.new(:connections => [
          Occam::Connection.new(:object => Occam::Object.new(
            :name => "simulator",
            :output_schema_document_id => output_schema.id.to_s
          ))
        ])

        workset = Occam::Workset.create(:name => "foo",
                                        :workflow => workflow,
                                        :account => a)

        Occam.any_instance.expects(:render).with(
          anything,
          :"results/show",
          anything
        )

        get "/worksets/#{workset.id}/results"
      end

      it "should pass the workset to the view" do
        a = login_as('wilkie')

        output_schema = Occam::OutputSchema.create

        workflow = Occam::Workflow.new(:connections => [
          Occam::Connection.new(:object => Occam::Object.new(
            :name => "simulator",
            :output_schema_document_id => output_schema.id.to_s
          ))
        ])

        workset = Occam::Workset.create(:name => "foo",
                                        :workflow => workflow,
                                        :account => a)

        Occam.any_instance.expects(:render).with(
          anything,
          anything,
          has_local(:workset, workset)
        )

        get "/worksets/#{workset.id}/results"
      end
    end

    describe "GET /worksets/:id/run" do
      it "should return 404 when the workset is not found" do
        Occam.any_instance.stubs(:markdown)

        get "/worksets/asdf/run"

        last_response.status.must_equal 404
      end

      it "should return 406 if workset is private and logged in user isn't owner/collaborator" do
        a = Occam::Account.create(:username => "wilkie",
                                  :password => "foo")

        output_schema = Occam::OutputSchema.create

        workflow = Occam::Workflow.new(:connections => [
          Occam::Connection.new(:object => Occam::Object.new(
            :name => "simulator",
            :output_schema_document_id => output_schema.id.to_s
          ))
        ])

        workset = Occam::Workset.create(:name     => "foo",
                                        :private  => 1,
                                        :workflow => workflow,
                                        :account  => a)

        login_as('jane')

        get "/worksets/#{workset.id}/run"

        last_response.status.must_equal 406
      end

      it "should return 406 if workset is private and nobody is logged in" do
        a = Occam::Account.create(:username => "wilkie",
                                  :password => "foo")

        output_schema = Occam::OutputSchema.create

        workflow = Occam::Workflow.new(:connections => [
          Occam::Connection.new(:object => Occam::Object.new(
            :name => "simulator",
            :output_schema_document_id => output_schema.id.to_s
          ))
        ])

        workset = Occam::Workset.create(:name => "foo",
                                        :private => 1,
                                        :workflow => workflow,
                                        :account => a)

        get "/worksets/#{workset.id}/run"

        last_response.status.must_equal 406
      end

      it "should return 302 upon success" do
        a = login_as('wilkie')

        output_schema = Occam::OutputSchema.create

        workflow = Occam::Workflow.new(:connections => [
          Occam::Connection.new(:object => Occam::Object.new(
            :name => "simulator",
            :output_schema_document_id => output_schema.id.to_s
          ))
        ])

        workset = Occam::Workset.create(:name => "foo",
                                        :workflow => workflow,
                                        :account => a)

        get "/worksets/#{workset.id}/run"

        last_response.status.must_equal 302
      end

      it "should redirect upon success to the workset page" do
        a = login_as('wilkie')

        output_schema = Occam::OutputSchema.create

        workflow = Occam::Workflow.new(:connections => [
          Occam::Connection.new(:object => Occam::Object.new(
            :name => "simulator",
            :output_schema_document_id => output_schema.id.to_s
          ))
        ])

        workset = Occam::Workset.create(:name => "foo",
                                        :workflow => workflow,
                                        :account => a)

        get "/worksets/#{workset.id}/run"

        last_response.location.must_equal(
          "http://example.org/worksets/#{workset.id}"
        )
      end

      it "should create a new Job for all unqueued Experiments in workset" do
        a = login_as('wilkie')

        output_schema = Occam::OutputSchema.create

        workflow = Occam::Workflow.new(:connections => [
          Occam::Connection.new(:object => Occam::Object.new(
            :name => "simulator",
            :output_schema_document_id => output_schema.id.to_s
          ))
        ])

        experiments = []

        experiments << Occam::Experiment.new(:name => "bar",
                                             :workflow => Occam::Workflow.new)

        experiments << Occam::Experiment.new(:name => "baz",
                                             :workflow => Occam::Workflow.new)

        experiments << Occam::Experiment.new(:name => "bat",
                                             :workflow => Occam::Workflow.new)

        Occam::Job.create(:experiment => experiments[1],
                          :kind       => "run",
                          :status     => "queued")

        workset = Occam::Workset.create(:name => "foo",
                                        :workflow => workflow,
                                        :experiments => experiments,
                                        :account => a)

        get "/worksets/#{workset.id}/run"

        Occam::Job.count.must_equal 3
      end

      it "should return JSON when json is more preferred than html" do
        a = login_as('wilkie')

        output_schema = Occam::OutputSchema.create

        workflow = Occam::Workflow.new(:connections => [
          Occam::Connection.new(:object => Occam::Object.new(
            :name => "simulator",
            :output_schema_document_id => output_schema.id.to_s
          ))
        ])

        workset = Occam::Workset.create(:name => "foo",
                                        :workflow => workflow,
                                        :account => a)

        accept "application/json"

        get "/worksets/#{workset.id}/run"

        content_type.must_equal "application/json"
      end
    end

    describe "GET /worksets/:id/cancel" do
      it "should return 404 when the workset is not found" do
        Occam.any_instance.stubs(:markdown)

        get "/worksets/asdf/cancel"

        last_response.status.must_equal 404
      end

      it "should return 406 if workset is private and logged in user isn't owner/collaborator" do
        a = Occam::Account.create(:username => "wilkie",
                                  :password => "foo")

        output_schema = Occam::OutputSchema.create

        workflow = Occam::Workflow.new(:connections => [
          Occam::Connection.new(:object => Occam::Object.new(
            :name => "simulator",
            :output_schema_document_id => output_schema.id.to_s
          ))
        ])

        workset = Occam::Workset.create(:name     => "foo",
                                        :private  => 1,
                                        :workflow => workflow,
                                        :account  => a)

        login_as('jane')

        get "/worksets/#{workset.id}/cancel"

        last_response.status.must_equal 406
      end

      it "should return 406 if workset is private and nobody is logged in" do
        a = Occam::Account.create(:username => "wilkie",
                                  :password => "foo")

        output_schema = Occam::OutputSchema.create

        workflow = Occam::Workflow.new(:connections => [
          Occam::Connection.new(:object => Occam::Object.new(
            :name => "simulator",
            :output_schema_document_id => output_schema.id.to_s
          ))
        ])

        workset = Occam::Workset.create(:name => "foo",
                                        :private => 1,
                                        :workflow => workflow,
                                        :account => a)

        get "/worksets/#{workset.id}/cancel"

        last_response.status.must_equal 406
      end

      it "should return 302 upon success" do
        a = login_as('wilkie')

        output_schema = Occam::OutputSchema.create

        workflow = Occam::Workflow.new(:connections => [
          Occam::Connection.new(:object => Occam::Object.new(
            :name => "simulator",
            :output_schema_document_id => output_schema.id.to_s
          ))
        ])

        workset = Occam::Workset.create(:name => "foo",
                                        :workflow => workflow,
                                        :account => a)

        get "/worksets/#{workset.id}/cancel"

        last_response.status.must_equal 302
      end

      it "should redirect upon success to the workset page" do
        a = login_as('wilkie')

        output_schema = Occam::OutputSchema.create

        workflow = Occam::Workflow.new(:connections => [
          Occam::Connection.new(:object => Occam::Object.new(
            :name => "simulator",
            :output_schema_document_id => output_schema.id.to_s
          ))
        ])

        workset = Occam::Workset.create(:name => "foo",
                                        :workflow => workflow,
                                        :account => a)

        get "/worksets/#{workset.id}/cancel"

        last_response.location.must_equal(
          "http://example.org/worksets/#{workset.id}"
        )
      end

      it "should destroy all Job records for all queued Experiments in workset" do
        a = login_as('wilkie')

        object = import_object(:name => 'somesim')

        workflow = Occam::Workflow.new(:connections => [
          Occam::Connection.new(:object => object)
        ])

        experiments = []

        experiments << Occam::Experiment.new(:name => "bar",
                                             :workflow => Occam::Workflow.new)

        experiments << Occam::Experiment.new(:name => "baz",
                                             :workflow => Occam::Workflow.new)

        experiments << Occam::Experiment.new(:name => "bat",
                                             :workflow => Occam::Workflow.new)

        Occam::Job.create(:experiment => experiments[1],
                          :kind       => "cancel",
                          :status     => "queued")

        workset = Occam::Workset.create(:name => "foo",
                                        :workflow => workflow,
                                        :experiments => experiments,
                                        :account => a)

        get "/worksets/#{workset.id}/cancel"

        Occam::Job.count.must_equal 0
      end

      it "should return JSON when json is more preferred than html" do
        a = login_as('wilkie')

        object = import_object(:name => 'somesim')

        workflow = Occam::Workflow.new(:connections => [
          Occam::Connection.new(:object => object)
        ])

        workset = Occam::Workset.create(:name => "foo",
                                        :workflow => workflow,
                                        :account => a)

        accept "application/json"

        get "/worksets/#{workset.id}/cancel"

        content_type.must_equal "application/json"
      end
    end
  end
end
