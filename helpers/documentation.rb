class Occam
  module DocumentationHelpers
    def documentation_nav(outline)
      return "" if outline.nil?
      if outline.url
        "<li><a href='#{outline.url}'>#{outline.text}</a><ul>#{documentation_nav(outline.child)}</ul></li>#{documentation_nav(outline.sibling)}"
      else
        "<li><a href='##{outline.slug}'>#{outline.text}</a><ul>#{documentation_nav(outline.child)}</ul></li>#{documentation_nav(outline.sibling)}"
      end
    end
  end

  helpers DocumentationHelpers
end
