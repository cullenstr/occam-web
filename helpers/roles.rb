class Occam
  set(:auth) do |*roles|
    condition do
      unless logged_in? && roles.any? {|role| current_account.has_role? role}
        redirect "/login", 303
      end
    end
  end
end
